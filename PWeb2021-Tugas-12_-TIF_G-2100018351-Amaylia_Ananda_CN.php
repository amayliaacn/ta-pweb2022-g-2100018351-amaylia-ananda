<?php
echo "<h2>- Array 01 -</h2>";
echo("Mendeklarasikan dan Menampilkan Array<br>");
$arrProdi=array("Informatika","Teknik Industri","Teknik Kimia","Teknik Elektro","Teknologi Pangan");
echo$arrProdi[0]."<br>";
echo$arrProdi[3]."<br>";
echo$arrProdi[4]."<br>";

$arrWarna=array();
$arrWarna[]="Pink";
$arrWarna[]="Biru";
$arrWarna[]="Kuning";
$arrWarna[]="Hijau";
$arrWarna[]="Putih";
$arrWarna[]="Coklat";
echo $arrWarna[0]. "<br>";
echo $arrWarna[1]. "<br>";
echo $arrWarna[5]. "<br>";
?>

<?php
echo "<br><h2>- Array 02 -</h2>";
echo("<br>Array Assosiatif (indeks selain angka)<br>");
$arrNilai=array("Cimol"=>80,"Bruno"=>90,"Kimzo"=>85);
echo $arrNilai['Cimol']."<br>";
echo $arrNilai['Kimzo']."<br><br>";

$arrNilai=array("<br>");
$arrNilai['Air']=80;
$arrNilai['Angin']=95;
$arrNilai['Api']=77;
echo$arrNilai['Angin']."<br>";
echo$arrNilai['Api']."<br>";
?>

<?php
echo "<br><h2>- Array 03 -</h2>";
echo("<br>Menampilkan Seluruh Isi Array dengan For dan Foreach");
$arrWarna=array("Purple","Green","Yellow","Pink","Blue","Pink","Red");

echo"Menampilkan isi arrray dengan For : <br>";
for($i=0;$i<count($arrWarna);$i++){
    echo"Warna Pelangi <font color=$arrWarna[$i]>".$arrWarna[$i]."</font><br>";
}

echo"<br>Menampilkan isi array dengan Foreach : <br>";
foreach($arrWarna as $warna){
    echo "Warna Pelangi <font color=$warna>".$warna."</font><br>";
}
?>

<?php
echo "<br><h2>- Array 04 -</h2>";
$arrNilai = array("Amaylia"=>80, "Ananda"=>90, "Cinta"=>75, "Naditya"=>85);
echo"Menampilkan seluruh isi array asosiatif dengan Foreach : <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama=$nilai<br>";
} 
reset($arrNilai);
echo"<br>Menampilkan seluruh isi array asosiatif dengan While-List : <br>";
while (list($nama ,$nilai)=each($arrNilai)) {
    echo "Nilai $nama=$nilai<br>";
}
?>

<?php
echo "<br><h2>- Array 05 -</h2>";
echo "Mencetak Struktur Array";
$arrWarna=array("Red","Orange","Yellow","Green","Blue","Purple");
$arrNilai=array("Amaylia"=>80, "Ananda"=>90, "Cinta"=>75, "Naditya"=>85);
echo "<pre>";
print_r($arrWarna);
echo "<br>";
print_r($arrNilai);
echo "</pre>";
?>

<?php
echo "<br><h2>- Array 06 -</h2>";
echo "Mengurutkan Array dengan Sort() & Rsort()<br>";
$arrNilai=array("Amaylia"=>80, "Ananda"=>90, "Cinta"=>75, "Naditya"=>85);
echo "<b>Array sebelum di urutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan Sort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan Rsort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>

<?php
echo "<br><h2>- Array 07 -</h2>";
echo "Mengurutkan Array dengan Asort() & Arsort()<br>";
$arrNilai=array("Amaylia"=>80, "Ananda"=>90, "Cinta"=>75, "Naditya"=>85);
echo "<b>Array sebelum di urutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

asort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan Asort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

arsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan Arsort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>

<?php
echo "<br><h2>- Array 08 -</h2>";
echo "Mengurutkan Array dengan Ksort() & Krsort()<br>";
$arrNilai=array("Amaylia"=>80, "Ananda"=>90, "Cinta"=>75, "Naditya"=>85);
echo "<b>Array sebelum di urutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

ksort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan Ksort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

krsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan Krsort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>

<?php
echo "<br><h2>- Array 09 -</h2>";
echo "Mengatur Posisi Pointer dalam Array<br>";
$transport=array('Mobil' , 'Motor' , 'Sepeda' , 'Kereta' , 'Pesawat');
echo "<pre>";
print_r($transport);
echo "</pre>";
$mode=current($transport);
echo $mode."<br>";
$mode=next($transport);
echo $mode."<br>";
$mode=current($transport);
echo $mode."<br>";
$mode=prev($transport);
echo $mode."<br>";
$mode=end($transport);
echo $mode."<br>";
$mode=current($transport);
echo $mode."<br>";
$mode=prev($transport);
echo $mode."<br>";
?>

<?php
echo "<br><h2>- Array 10 -</h2>";
echo "Mencari Elemen Array<br>";
$arrBuah=array("Belimbing" , "Apel" , "Durian" , "Sawo" , "Anggur");
if(in_array("Kedondong",$arrBuah)){
    echo "Ada buah kedondong di dalam array tersebut!";
}else{
    echo "Tidak ada buah kedondong di array tersebut!";
}
?>



