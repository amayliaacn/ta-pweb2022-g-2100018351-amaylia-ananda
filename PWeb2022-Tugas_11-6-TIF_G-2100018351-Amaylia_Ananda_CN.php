<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pengolahan Form type input Checkbox</title>
</head>
<body>
    <form action="" method="POST" name="input">
        <h2>Genre Film Favorit</h2>
        <input type="checkbox" name="film1" value="Drama" checked> Drama <br>
        <input type="checkbox" name="film2" value="Comedy"> Comedy <br>
        <input type="checkbox" name="film3" value="Horror"> Horror <br>
        <input type="checkbox" name="film4" value="Action"> Action <br>
        <input type="checkbox" name="film5" value="Romance"> Romance <br>
        <input type="checkbox" name="film6" value="Science Fiction (Sci - Fi)"> Science Fiction (Sci - Fi) <br>
        <input type="submit" name="Pilih" value="Pilih">
        <input type="submit" name="reset" value="Reset">
    </form>
</body>
</html>

<?php
if (isset($_POST['Pilih'])){
    echo "<br>Genre Favorit Anda adalah : <br/>";
    if(isset($_POST['film1'])){
        echo "-" .$_POST['film1'] . "<br>";
    }if(isset($_POST['film2'])){
        echo "-" .$_POST['film2'] . "<br>";
    }if(isset($_POST['film3'])){
        echo "-" .$_POST['film3'] . "<br>";
    }if(isset($_POST['film4'])){
        echo "-" .$_POST['film4'] . "<br>";
    }if(isset($_POST['film5'])){
        echo "-" .$_POST['film5'] . "<br>";
    }if(isset($_POST['film6'])){ 
        echo "-" .$_POST['film6'] . "<br>";
    }
}
?>