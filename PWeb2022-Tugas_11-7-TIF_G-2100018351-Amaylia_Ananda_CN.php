<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pengolahan Form type input Combo Box</title>
</head>
<body>
    <form action="" method="post" name="input">
        <h2>Program Studi</h2>
        <select name="prodi">
            <option value="Informatika"> Informatika <br></option>
            <option value="Teknik Elektro"> Teknik Elektro <br></option>
            <option value="Teknik Industri"> Teknik Industri <br></option>
            <option value="Teknik Kimia"> Teknik Kimia <br></option>
            <option value="Teknologi Pangan"> Teknologi Pangan <br></option>
        </select>
        <input type="submit" name="Pilih" value="Pilih">
    </form>
</body>
</html>

<?php
if(isset($_POST['Pilih'])){
    $prodi=$_POST['prodi'];
    echo "Program Studi Anda adalah : <font color=powder blue>$prodi</font>";
}
?>