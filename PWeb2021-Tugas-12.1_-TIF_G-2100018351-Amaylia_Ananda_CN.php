<?php
echo "<h2>- Fungsi 01 -</h2><br>";
echo "Fungsi Tanpa Return Value & Parameter<br>";
function cetak_ganjil(){
    for($i = 0; $i < 100; $i++){
        if($i%2 == 1){
            echo "$i, ";
        }
    }
}
cetak_ganjil();
echo "<br><br><br>";
?>

<?php
echo "<h2>- Fungsi 02 -</h2><br>";
echo "Fungsi Tanpa Return Value tetapi dengan Parameter<br>";
function cetak_ganjil2($awal, $akhir){
    for($i = $awal; $i <$akhir; $i++){
        if($i%2 == 1){
            echo "$i, ";
        }
    }
}
$a = 10;
$b = 50;
echo "<b>Bilangan ganjil dari $a sampai $b adalah : </b><br>";
cetak_ganjil2($a, $b);
echo "<br><br><br>";
?>

<?php
echo "<h2>- Fungsi 03 -</h2><br>";
echo "Fungsi dengan Return Value dan Parameter<br>";
function kel_lingkaran($jari){
    return 2*3.14*$jari;
}

$r=14;
echo "Keliling Lingkaran dengan jari - jari $r = ";
echo kel_lingkaran($r);
?>

<?php
echo "<h2>- Fungsi 04 -</h2><br>";
echo "Passing by Value<br>";
function tambah_string($str){
    $str=$str. ",Yogyakarta";
    return $str;
}

$string="Universitas Ahmad Dahlan";
echo "\$string = $string <br>";
echo tambah_string($string)."<br>";
echo "\$string = $string<br>";
?>

<?php
echo "<h2>- Fungsi 05 -</h2><br>";
echo "Passing by Reference<br>";
function tambah_string2(&$str){
    $str=$str. ",Yogyakarta";
    return $str;
}

$string="Universitas Ahmad Dahlan";
echo "\$string = $string <br>";
echo tambah_string2($string). "<br>";
echo "\$string = $string<br>";
?>

<?php
echo "<h2>- Fungsi 06 -</h2><br>";
echo "Tampilkan UDF dan Fungsi yang Didukung PHP Versi Saat Ini<br>";
function kel_lingkaran2($jari){
    return 2*3.14*$jari;
}
$arr=get_defined_functions();
echo "<pre>";
print_r($arr);
echo "</pre>";
?>

<?php
echo "<h2>- Fungsi 07 -</h2><br>";
echo "Cek Keberadaan Fungsi di Versi PHP Saat Ini<br>";
function kel_lingkaran3($jari){
    return 2*3.14*$jari;
}

$arr=get_defined_functions();
if(in_array("tambah_string",$arr)){
    echo "Fungsi exif_read_data ada di PHP versi ini";
}else{
    echo "Tidak ada exif_read_data di array tersebut!";
}
?>