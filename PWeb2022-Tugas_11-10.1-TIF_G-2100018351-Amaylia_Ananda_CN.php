<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validasi Form</title>
    <style>
        .error{
            color : red;
        }
    </style>
</head>
<body>
    <?php
    $nameErr    = $nimErr = $emailErr = $alamatErr = "";
    $name       = $nim    = $email    = $alamat    = "";
    
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        if(empty($_POST["name"])){
            $nameErr = "Name is requied";
        }
        else{
            $name = test_input($_POST["name"]);
            if(!preg_match("/^[a-zA-Z]*$/", $name)){
                $nameErr = "Only letter and white space allowed";
            }
        }

        if(empty($_POST["nim"])){
            $nimErr = "NIM is requied";
        }
        else{
            $nim = test_input($_POST["nim"]);
        }

        if(empty($_POST["email"])){
            $emailErr = "Email is requied";
        }
        else{
            $email = test_input($_POST["Email"]);
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $emailErr = "Invalid email format";
            }
        }

        if(empty($_POST["alamat"])){
            $alamatErr = "Alamat is requied";
        }
        else{
            $alamat = test_input($_POST["alamat"]);
        }
   }
    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>

    <form method="post" action="" >
        Nama    : <input type="text" name="name" value="<?php echo $name;?>">
        <span class="error">
            *<?php echo $nameErr;?>
        </span>
        <br><br>
        NIM     : <input type="text" name="nim" value="<?php echo $nim;?>">
        <span class="error">
            *<?php echo $nimErr;?>
        </span>
        <br><br>
        E-mail  : <input type="text" name="email" value="<?php echo $email;?>">
        <span class="error">
            *<?php echo $emailErr;?>
        </span>
        <br><br>
        Alamat  : <input type="text" name="alamat" value="<?php echo $alamat;?>">
        <span class="error">
            *<?php echo $alamatErr;?>
        </span>
        <br><br>
        <input type="submit" name="Submit" value="Submit">
        <input type="submit" name="reset" value="Reset">
    </form>

    <?php
    echo "<h2>Your Input : </h2>";
    echo $name;
    echo "<br>";
    echo $nim;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $alamat;
    echo "<br>"
?>
</body>
</html>