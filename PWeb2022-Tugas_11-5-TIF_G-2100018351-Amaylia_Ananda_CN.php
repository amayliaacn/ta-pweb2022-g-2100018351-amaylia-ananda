<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pengolahan Form type input Type Radio</title>
</head>
<body>
    <form action="" method="POST" name="input">
        <h2>Pilih Olahraga Favorit</h2>
        <input type="radio" name="olahraga" value="Lari" checked/> Lari <br>
        <input type="radio" name="olahraga" value="Sepeda"> Sepeda <br>
        <input type="radio" name="olahraga" value="Renang"> Renang <br>
        <input type="radio" name="olahraga" value="Basket"> Basket <br>
        <input type="radio" name="olahraga" value="Voli"> Voli <br>
        <input type="radio" name="olahraga" value="Sepak Bola"> Sepak Bola <br>
        <input type="submit" name="Pilih" value="Pilih">
        <input type="submit" name="reset" value="Reset">
    </form>
</body>
</html>

<?php
if(isset($_POST['Pilih'])){
    $or=$_POST['olahraga'];
    echo "Olahraga favorit anda adalah : $or";
}
?>