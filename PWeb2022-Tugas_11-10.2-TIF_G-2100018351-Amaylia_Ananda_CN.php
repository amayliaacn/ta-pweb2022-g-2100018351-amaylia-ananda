<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validasi Form dengan JavaScript</title>
    <style type="text/css">
        body {
            background-color: salmon;
            font-family: sans-serif;
        }

        .form {
            padding: 1em;
            margin: 1em auto;
            width: 17em;
            background: #D8BFD8;
            border-radius: 3px;
            text-align: justify;
        }
    </style>
</head>
<body>
    <div class="form">
        <h1 align="center"><BR>Input Data Diri</h1>
        <FORM NAME="fform">
            <PRE>
      Nama   :  <input type="text" size="11" name="nama" id="nama"><br>
      NIM    :  <input type="text" size="11" name="nim" id="nim"><br>
      Alamat :  <input type="text" size="11" name="alamat" id="alamat"><br>
      Email  :  <input type="email" size="11" name="email" id="email">
            </PRE>
            <P>
                <INPUT TYPE="button" value="Simpan" onclick="data()">
                <INPUT TYPE="reset" value="Ulang">
        </FORM>
    </div>
</body>
    <script type="text/javascript">
        function data(){
            var nama = document.getElementById("nama").value;
            var nim = document.getElementById("nim").value;
            var alamat = document.getElementById("alamat").value;
            var email = document.getElementById("email").value;
        
            if (nama != "" && nim != "" && alamat != "" && email != ""){
             return true;
            }
            else{
             alert('Harap isi data dengan lengkap!');
        }
    }      
</script>
</html>